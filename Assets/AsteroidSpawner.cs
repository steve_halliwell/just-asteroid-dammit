﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawner : MonoBehaviour {

	public GameObject astPre;

	public float minRate, maxRate;
	public float radius;

	private float curRate, counter;

	public float horScale, verScale;

	void Start()
	{
		PickNextRate();
	}
	
	// Update is called once per frame
	void Update () {

		counter += Time.deltaTime;

		if(counter >= curRate)
		{
			Vector3 spawnPos = transform.position + new Vector3(Random.Range(-horScale,horScale), Random.Range(-verScale,verScale),0);
			Vector3 target = Random.onUnitSphere * radius;
			target.z = 0;
			Quaternion spawnRot = Quaternion.FromToRotation(Vector3.up, target - spawnPos);

			Instantiate(astPre, spawnPos, spawnRot);
			PickNextRate();
		}
	
	}

	void PickNextRate()
	{
		curRate = Random.Range(minRate, maxRate);
		counter = 0;
	}
}
