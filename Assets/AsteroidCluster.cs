﻿using UnityEngine;
using System.Collections;

public class AsteroidCluster : MonoBehaviour {

	public int min, max;
	public float minImp, maxImp;

	public GameObject instPre;

	// Use this for initialization
	void Start () {
		int rnd = Random.Range(min, max);

		for(int i = 0; i < rnd; i++)
		{
			GameObject newGo = (GameObject)Instantiate(instPre, transform.position, transform.rotation);

			newGo.rigidbody2D.AddForce(Random.insideUnitCircle.normalized * Random.Range(minImp, maxImp));
		}

		Destroy (gameObject);
	}
}
