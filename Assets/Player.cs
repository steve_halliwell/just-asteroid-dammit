﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float forwardForce;
	public float rotateForce;
	public float dragScale, angDragScale;
	public float startingDrag, startingAngDrag;
	public float dragStartsAt, angDragStartsAt;

	public GameObject bulletPre;
	public float fireRate;
	public float bulletImpulse;
	private float counter = 0;

	public KeyCode fireKey;

	public Transform bulletSpawnLoc;

	private Rigidbody2D rb;

	void Start()
	{
		rb = rigidbody2D;
	}

	void FixedUpdate()
	{
		rb.AddRelativeForce(Vector2.up * forwardForce * Input.GetAxis("Vertical"));
		rb.AddTorque(-rotateForce * Input.GetAxis("Horizontal"));
		if(dragStartsAt < rb.velocity.magnitude)
			rb.drag = (rb.velocity.magnitude - dragStartsAt) * dragScale + startingDrag;
		else
			rb.drag = startingDrag;

		if(angDragStartsAt < Mathf.Abs(rb.angularVelocity))
			rb.angularDrag = (Mathf.Abs(rb.angularVelocity) - angDragStartsAt) * angDragScale + startingAngDrag;
		else
			rb.angularDrag = startingAngDrag;

		counter += Time.deltaTime;

		if(Input.GetKey(fireKey) && counter >= fireRate)
		{
			counter = 0;
			GameObject newBul = (GameObject)Instantiate(bulletPre, bulletSpawnLoc.position, transform.rotation);
			newBul.rigidbody2D.AddRelativeForce(Vector2.up * bulletImpulse, ForceMode2D.Impulse);
		}
	}
}
