﻿using UnityEngine;
using System.Collections;

public class WorldWrap : MonoBehaviour {

	public Vector3 dir;
	public bool xflip = true;

	void OnTriggerExit2D(Collider2D col)
	{
		if(Vector2.Dot(col.rigidbody2D.velocity, dir) > 0)
		{
			if(xflip)
			{
				float x = col.transform.position.x;
				x *= -1;
				col.transform.position = new Vector3(x,col.transform.position.y,0);
			}
			else
			{
				float y = col.transform.position.y;
				y *= -1;
				col.transform.position = new Vector3(col.transform.position.x,y,0);
			}
		}
	}
}
