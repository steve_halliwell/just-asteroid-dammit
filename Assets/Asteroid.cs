﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {

	public float hp;

	public float speedMin, speedMax;

	public GameObject spawnOnDie;

	void Start()
	{
		rigidbody2D.AddRelativeForce(Vector2.up * Random.Range(speedMin, speedMax) * rigidbody2D.mass, ForceMode2D.Impulse);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag == "Bullet")
		{
			OnTakeDamage(1.0f);
			
			Destroy(col.gameObject);
		}
	}

	void OnTakeDamage(float dmg)
	{
		hp-=dmg;

		if(hp <= 0)
		{
			OnDie();
		}
	}

	void OnDie()
	{
		if(spawnOnDie != null)
		{
			Instantiate(spawnOnDie, transform.position, transform.rotation);
		}
		Destroy(gameObject);
	}
}
