﻿using UnityEngine;
using System.Collections;

public class DieOnRespawnTrigger : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Respawn")
			Destroy(gameObject);
	}
}
